import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pictureView: UIImageView!
    
    func configure(object: UIImage ) {
        pictureView.image = object
        pictureView.contentMode = .scaleAspectFill
    }
}

