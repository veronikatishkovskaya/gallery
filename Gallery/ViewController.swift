import Foundation
import UIKit
import Photos

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addPhotoOutlet: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var fastScrollOutlet: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let imagePicker = UIImagePickerController()
    let cameraView = UIImageView()
    var photoArray: [UIImage] = []
    var isScrolling = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fastScrollOutlet.isHidden = true
        checkPhotoLibraryPermission()
        mainViewSettings()
        cameraViewCellSettings()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isScrolling == true {
            scrollToBottom(animated: false)
        }
        isScrolling = false
    }
    
    @IBAction func addNewPhoto(_ sender: UIButton) {
        performImagePicker()
    }
    
    @IBAction func fastScroll(_ sender: UIButton) {
        scrollToBottom(animated: true)
    }
    
    private func performImagePicker() {
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .currentContext
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    private func performCameraPicker() {
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.cameraDevice = .rear
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    func cameraViewCellSettings() {
        cameraView.image = UIImage(named: "camera")
        cameraView.contentMode = .scaleAspectFit
        photoArray.insert(cameraView.image!, at: photoArray.count)
    }
    
    func mainViewSettings() {
        addPhotoOutlet.layer.cornerRadius = 24
        bottomView.layer.borderWidth = 2
        bottomView.layer.borderColor = #colorLiteral(red: 0.8082696795, green: 0.7957677841, blue: 1, alpha: 1)
        addPhotoOutlet.titleLabel?.textAlignment = .center
    }
    
    func scrollToBottom (animated: Bool) {
        let lastItemIndex = IndexPath(item: self.photoArray.count - 1, section: 0)
        self.collectionView?.scrollToItem(at: lastItemIndex, at: .bottom, animated: animated)
    }
    
    func loadMediaItems() {
        DispatchQueue.main.async {
            self.startLoading()
            let imageManager = PHImageManager.default()
            let imageRequestOptions = PHImageRequestOptions()
            
            imageRequestOptions.isSynchronous = true
            imageRequestOptions.deliveryMode = .highQualityFormat
            
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            fetchOptions.fetchLimit = 100
            
            let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
            if fetchResult.count > 0 {
                for index in 0...fetchResult.count - 1 {
                    imageManager.requestImage(for: fetchResult.object(at: index) , targetSize: CGSize(width: 3000, height: 4000), contentMode: .aspectFill, options: imageRequestOptions) { (image, error) in
                        self.photoArray.insert(image!, at: 0)
                    }
                }
                self.finishLoading()
                self.collectionView.reloadData()
            } else {
                print("no media")
            }
        }
        
    }
    
    func checkPhotoLibraryPermission() {
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized,
                 .limited:
                self.loadMediaItems()
            case .denied,
                 .restricted:
                print("no access")
            default:
                print("ask for permission")
            }
            
        }
    }
    
    func startLoading() {
        self.activityIndicator.startAnimating()
        self.view.addSubview(self.activityIndicator)
    }
    
    func finishLoading() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.hidesWhenStopped = true
    }
    
}
extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let pickedImage = info[.originalImage] as? UIImage else {return}
        photoArray.insert(pickedImage, at: photoArray.count - 1)
        picker.dismiss(animated: true, completion: nil)
        
        self.collectionView.reloadData()
        self.collectionView.performBatchUpdates(nil, completion: {
            (result) in
            self.scrollToBottom(animated: false)
        })
    }
    
    
}
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as? PhotoCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.configure(object: photoArray[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "PictureViewController") as! PictureViewController
        
        controller.image = photoArray[indexPath.row]
        if controller.image == UIImage(named: "camera") {
            performCameraPicker()
        } else {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 100)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        fastScrollOutlet.isHidden = false
    }
    
}

