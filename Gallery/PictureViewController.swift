import UIKit

class PictureViewController: UIViewController {
    
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var backButtonOutlet: UIButton!
    
    var image = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButtonOutlet.layer.cornerRadius = 15
        pictureView.image = image
        pictureView.contentMode = .scaleAspectFit
    }
    
    @IBAction func goBackToGallery(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
